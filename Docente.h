//
// Created by jordy on 12/04/17.
//
#ifndef ALGORITMOGENETICO_DOCENTE_H
#define ALGORITMOGENETICO_DOCENTE_H
#include "Disciplina.h"
#include "vector"
using namespace std;

class Docente{
    int id;
	vector<int> disciplinas;
    vector<Disciplina> disciplinasMinistraveis;

public:

    Docente(int id, const vector<Disciplina> &disciplinasMinistraveis) : id(id), disciplinasMinistraveis(
            disciplinasMinistraveis) {}
    virtual ~Docente() {}
	Docente(int id, vector<int> disciplinas) : id(id), disciplinas(disciplinas){}


    int getId() const {
        return id;
    }
    void setId(int id) {
        Docente::id = id;
    }
	vector<int> get_disciplinas() {return disciplinas;}
    const vector<Disciplina> &getDisciplinasMinistraveis() const {
        return disciplinasMinistraveis;
    }
    void setDisciplinasMinistraveis(const vector<Disciplina> &disciplinasMinistraveis) {
        Docente::disciplinasMinistraveis = disciplinasMinistraveis;
    }
};
#endif //ALGORITMOGENETICO_DOCENTE_H
