#include <sstream>
#include "Ag.h"
#include "file_to_object.h"

using namespace std;


int main() {
	int contador = 0;
	int geracoes;                                   //numero de gerações
	double mutacao;                                 //taxa de mutação
	double recombinacao;                            //taxa de recombinação
    int tamanho_da_populacao;                       //tamanho da população inicial

    // ========== Recebimento dos dados =======
	File_to_object fto("ag-informacoes.csv");
	fto.file_to_object();

/**    Teste dos dados*/
//    stringstream temp;
//    while (!fto.getSalas().empty()) {
//        temp << fto.getSalas().back().getId();
//    }


    // ========== Entradas do usuário =========
    cout << "Ensira o número de gerações:" << endl;
    cin >> geracoes;
    cout << "Ensira a taxa de mutação:" << endl;
    cin >> mutacao;
    cout << "Ensira a taxa de recombinação:" << endl;
    cin >> recombinacao;
    cout << "Ensira o tamanho da população inicial:" << endl;
    cin >> tamanho_da_populacao;



    // ========== Execução da AG ==============
    Ag ag = Ag(geracoes,mutacao,recombinacao,
               fto.list_to_vector_docente(),
               fto.list_to_vector_estudante(),
               fto.list_to_vector_sala(),
			   fto.getDisciplinas(),
			   fto.getTimeslots());

    cout << "Ag instanciada" << endl;

    while (contador <= geracoes){
        ag.gerarPopulacao(tamanho_da_populacao);
        cout << "população gerou " << ag.getNum_individuos() << endl;
        ag.reproduzir();
        cout << "população reproduziu " << ag.getNum_individuos() << endl;
        ag.mutarCromossomo();
        cout << "população mutou" << ag.getNum_individuos() << endl;
        geracoes++;
    }

    // ========== Saída =====================
    Cromossomo melhor = ag.retorna_melhor();
    cout << melhor.toString();
    //TODO Testar saída
    ofstream output ("saida.csv");
    if (output.is_open())
    {
        output << melhor.toString();
        output.close();
    }
    else cout << "Unable to open file";

	return 0;
}

