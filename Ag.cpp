//
// Created by jordy on 04/04/17.
//

#include <iostream>
#include <algorithm>
#include "Ag.h"

using namespace std;

//Constructor
Ag::Ag(int geracao, double mutacao, double recombinacao, vector<Docente> *docentes_disponiveis,
       vector<Estudante> *estudantes_disponiveis, vector<Sala> *salas_disponiveis,vector<Disciplina> disciplinas, string** timeslots)
        : num_individuos(num_individuos), geracao(geracao), mutacao(mutacao), recombinacao(recombinacao),
          docentes_disponiveis(docentes_disponiveis), estudantes_disponiveis(estudantes_disponiveis),
          salas_disponiveis(salas_disponiveis), disciplinas(disciplinas), lista_timeslot(timeslots) {}

//Destructor
Ag::~Ag() {

}


/**É gerado uma população de cromossomos validos de tamanho definido pelo usuário*/
void Ag::gerarPopulacao(int tamanho_da_populacao){
    //TODO testar
    num_individuos = 0;
    while (num_individuos < tamanho_da_populacao){

        vector<Elemento>* horario = criaIndividuo();// Cria o cromossomo viável

        Cromossomo cromossomo = Cromossomo(horario);
        populacao.push_back(cromossomo);            // Acrescente individuos à população
        num_individuos++;
    }
}

int converter_horas(string string_horas) {
	string horas;
   	horas += string_horas[0];
   	horas += string_horas[1];
	return stoi(horas);
}

/**É gerado um vetor do tamanho MAX_TIMESLOT no qual terá uma lista em cada índice do vetor.
     A lista é composta por elementos, e os elementos possuem (Professor, Sala, Turma, Disciplina e uma lista de alunos)
     Cada elemento só entrará na lista se ele for válido, ou seja, se não violar as hard constrains
     Para isso são utilizadas listas de professores, salas e alunos disponives no horário determinado.*/
//TODO Testar
vector<Elemento>* Ag::criaIndividuo(){
    vector<Elemento>* horario =  new vector<Elemento>[MAX_TIMESLOT];
    int i = 0;              // Iterador do vetor de time slots
    int contador = 0;       // Elementos adicionados por iteração
    do{

        //TODO Criar metodo que transforma 7:00 para 7
        if(stoi(lista_timeslot[i][1]) != 1 && stoi(lista_timeslot[i][1]) != 7 &&  // Ignorar sabados e domingos
                converter_horas(lista_timeslot[i][2]) > 7 &&          // Ignorar antes das 7:00
                converter_horas(lista_timeslot[i][2]) < 22 &&         // Ignorar depois das 22:00
                converter_horas(lista_timeslot[i][2]) != 12 &&        // Ignorar 12:00
                converter_horas(lista_timeslot[i][2]) != 18){         // Ignorar 18:00

            if(i == MAX_TIMESLOT && contador == 0) {
                //Se chegar no final do vetor e não conseguiu inserir nenhum elemento na lista então ele finaliza a interação
                //Indivíduo pronto
                break;
            }else if(i == MAX_TIMESLOT && contador != 0){             // Recomeça a iteração
                contador = 0;
                i = 0;
            }else {
                //Cria elemento
                if(!docentes_disponiveis[i].empty() && !salas_disponiveis[i].empty()){//Tenta inserir individuo na lista
                    //Docente
                    Docente docente = docentes_disponiveis[i].back();
                    docentes_disponiveis[i].pop_back();

                    //Disciplina
					Disciplina *disciplina;
					vector<int> disciplinas_id = docente.get_disciplinas();
					srand(time(0));
					int random_disc = rand() % disciplinas_id.size();
					int disc_id = disciplinas_id.at(random_disc);
					for(int j = 0; j < disciplinas.size(); j++) {
						if(disciplinas.at(j).getId() == disc_id) {
							cout << disc_id  << endl;
							*disciplina = disciplinas.at(j);
							disciplinas.erase(disciplinas.begin() + j);
							break;
						}
					}

//                    Turma turma = disciplina.getTurma();
                    vector<Estudante> estudantes;//TODO acrescentar os alunos
                    Sala sala = salas_disponiveis[i].back();
					salas_disponiveis[i].pop_back();
                    Elemento elemento = Elemento(*disciplina,sala,estudantes,docente/*,turma*/);

                    //Insere elemento no indivíduo
                    horario[i].push_back(elemento);
					cout << "Adicionado na timeslot " << i  << endl;
                    i++;
                    contador++;
                }else{ // Se não , vai pra proxima posição do vetor
                    i++;
                }
            }
        } else i++;

    } while(i < MAX_TIMESLOT);
    return horario;
}





void Ag::reproduzir(){
	/** Cria copia dos pais, troca as listas dos timeslot pares e então substitui esses filhos pelos piores da população*/
    //TODO testar
	for (int j = 0; j < (TAMANHO_POPUPALCAO*this->recombinacao); j++){
		Cromossomo pai = aleatorio_melhores();
		Cromossomo mae = aleatorio_melhores();

		for(int i = 0; i < MAX_TIMESLOT; i = i+2) {
			swap(pai.getHorario()[i],mae.getHorario()[i]); //Troca as listas entre os cromossomos pai e mãe
		}
	}
}

void Ag::mutarCromossomo(){
    /**Este método vai receber a probabilidade de ocorrer a mutação na população e calcular quantos individuos vão mutar
     Com isso irá gerá dois numeros aleatórios para saber quem irá mutar
     Sabendo que indivíduos iram mutar, é gerado outros dois numeros aleatórios para saber qual posição do vetor será trocada*/
    //TODO Testar
    int n_mutacoes = (int) this->mutacao*TAMANHO_POPUPALCAO/100;
    for (int i = 0; i < n_mutacoes; ++i) {
        int induviduo =  rand() % TAMANHO_POPUPALCAO;
        int time_slot1 =  rand() % MAX_TIMESLOT;
        int time_slot2 =  rand() % MAX_TIMESLOT;
        std::swap(populacao[induviduo].getHorario()[time_slot1],populacao[induviduo].getHorario()[time_slot2]);
    }
}

/**Retorna um cromossomo aleatório dentre os melhores*/
Cromossomo Ag::aleatorio_melhores(){
    //TODO testar
    ordenar_por_fitness();
    int num_aleatorio =  rand() % TAMANHO_MELHORES;
    return populacao[num_aleatorio];
}

bool Ag::wayToSort(Cromossomo cromossomo1, Cromossomo cromossomo2) {
    return cromossomo1.gerarFitness() > cromossomo2.gerarFitness();
}

/**Retorna um lista dos melhores indivíduos ordenados */
vector<Cromossomo> Ag::ordenar_por_fitness(){
    //TODO testar
 	//ordena a lista pelo fitness
    std::sort (populacao.begin(), populacao.end(), [this](Cromossomo c1, Cromossomo c2) { return wayToSort(c1, c2); });
    vector<Cromossomo> lista_melhores;
    for(int i=0;i<TAMANHO_MELHORES;i++){
        lista_melhores[i] = populacao[i];
    }
    return lista_melhores;
}

/**Retorna o melhor individuo da população*/
Cromossomo Ag::retorna_melhor(){
    //TODO Testar
    ordenar_por_fitness();
    return populacao.front();
}



//=================== Getters and Setters ==============================

int Ag::getGeracao() const {
    return geracao;
}
void Ag::setGeracao(int geracao) {
    Ag::geracao = geracao;
}
double Ag::getMutacao() const {
    return mutacao;
}
void Ag::setMutacao(double mutacao) {
    Ag::mutacao = mutacao;
}
double Ag::getRecombinacao() const {
    return recombinacao;
}
void Ag::setRecombinacao(double recombinacao) {
    Ag::recombinacao = recombinacao;
}

int Ag::getNum_individuos() const {
    return num_individuos;
}

void Ag::setNum_individuos(int num_individuos) {
    Ag::num_individuos = num_individuos;
}

vector<Docente> *Ag::getDocentes_disponiveis() const {
    return docentes_disponiveis;
}

void Ag::setDocentes_disponiveis(vector<Docente> *docentes_disponiveis) {
    Ag::docentes_disponiveis = docentes_disponiveis;
}

vector<Estudante> *Ag::getEstudantes_disponiveis() const {
    return estudantes_disponiveis;
}

void Ag::setEstudantes_disponiveis(vector<Estudante> *estudantes_disponiveis) {
    Ag::estudantes_disponiveis = estudantes_disponiveis;
}

vector<Sala> *Ag::getSalas_disponiveis() const {
    return salas_disponiveis;
}

void Ag::setSalas_disponiveis(vector<Sala> *salas_disponiveis) {
    Ag::salas_disponiveis = salas_disponiveis;
}

const vector<Cromossomo> &Ag::getPopulacao() const {
    return populacao;
}

void Ag::setPopulacao(const vector<Cromossomo> &populacao) {
    Ag::populacao = populacao;
}
