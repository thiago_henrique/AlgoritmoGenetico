//
// Created by jordy on 12/04/17.
//

#ifndef ALGORITMOGENETICO_DISCIPLINA_H
#define ALGORITMOGENETICO_DISCIPLINA_H

#include "Turma.h"
#include "vector"

class Disciplina{

private:
    int id;
	int curso_id;
	int periodo;
    int carga_horaria_teorica;
	int tipo_t;
    int carga_horaria_pratica;
	int tipo_p;
//	vector<int> restricoes;         // horários permitidos para a Disciplina
//	Turma turma;

public:
//	Disciplina(int id, int carga_horaria_teorica, int carga_horaria_pratica, const vector<int> &restricoes, const Turma &turma);
    Disciplina(int id, int curso_id, int periodo, int cht, int tipo_t, int chp, int tipo_p) : id(id), curso_id(curso_id), periodo(periodo), carga_horaria_teorica(cht), tipo_t(tipo_t), carga_horaria_pratica(chp), tipo_p(tipo_p) {}

    virtual ~Disciplina();

    int getId() const;

    void setId(int id);

    int getCarga_horaria_teorica() const;

    void setCarga_horaria_teorica(int carga_horaria_teorica);

    int getCarga_horaria_pratica() const;

    void setCarga_horaria_pratica(int carga_horaria_pratica);

    const vector<int> &getRestricoes() const;

//	void setRestricoes(const vector<int> &restricoes);

    const Turma &getTurma() const;

//	void setTurma(const Turma &turma);





};

#endif //ALGORITMOGENETICO_DISCIPLINA_H
