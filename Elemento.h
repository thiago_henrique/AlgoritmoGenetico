 
//
// Created by jordy on 12/04/17.
//

#include "vector"
#include "Disciplina.h"
#include "Sala.h"
#include "Estudante.h"
#include "Turma.h"
#include "Docente.h"

#ifndef ALGORITMOGENETICO_ELEMENTO_H
#define ALGORITMOGENETICO_ELEMENTO_H
using namespace std;

class Elemento{

private:
    Disciplina disciplina;
    Sala sala;
    vector<Estudante> estudantes;
    Docente docente;
//	Turma turma;

public:
    Elemento(const Disciplina &disciplina, const Sala &sala, const vector<Estudante> &estudantes,
             const Docente &docente/*, const Turma &turma*/);
    virtual ~Elemento();

    const Disciplina &getDisciplina() const;

    void setDisciplina(const Disciplina &disciplina);

    const Sala &getSala() const;

    void setSala(const Sala &sala);

    const vector<Estudante> &getEstudantes() const;

    void setEstudantes(const vector<Estudante> &estudantes);

    const Docente &getDocente() const;

    void setDocente(const Docente &docente);

//	const Turma &getTurma() const;

//	void setTurma(const Turma &turma);

};
#endif 
