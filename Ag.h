//
// Created by jordy on 04/04/17.
//

#ifndef ALGORITMOGENETICO_AG_H
#define ALGORITMOGENETICO_AG_H

#include "Docente.h"
#include "Estudante.h"
#include "Cromossomo.h"
#include "Disciplina.h"


#define TAMANHO_POPUPALCAO 100
#define TAMANHO_MELHORES 50


class Ag {
    int num_individuos; //numero de cromossomos
    int geracao;        //numero de gerações
    double mutacao;     //taxa de mutação
    double recombinacao;//taxa de recombinação

    vector<Docente>* docentes_disponiveis =  new vector<Docente>[MAX_TIMESLOT];          // Lista de docentes disponíveis por TIMESLOT
    vector<Estudante>* estudantes_disponiveis =  new vector<Estudante>[MAX_TIMESLOT];    // Lista de estudantes disponíveis por TIMESLOT
    vector<Sala>* salas_disponiveis =  new vector<Sala>[MAX_TIMESLOT];                   // Lista de salas disponíveis por TIMESLOT
    vector<Cromossomo> populacao;                                                        // Lista de todos os cromossomos da população
	vector<Disciplina> disciplinas;
	string** lista_timeslot;

public:


    //====================Metodos do algoritmo genétio ======================

    void gerarPopulacao(int tamanho_da_populacao);                 // Gera uma população de vetores de listas de tamanho definido pelo usuario
    void reproduzir();                                             // Crossing Over baseado na troca das posições do vetor de timeslots
    void mutarCromossomo();                                        // Intercala listas do vetor verticalmente
    Cromossomo aleatorio_melhores();                               // Retorna um cromossomo aleatoriamente da lista dos melhores cromossomos
    vector<Cromossomo> ordenar_por_fitness();                      // Ordena a população de acordo com os fitness
    bool wayToSort(Cromossomo cromossomo1, Cromossomo cromossomo2);// Como deve ser feito a ordenação
    Cromossomo retorna_melhor();                                   // Retorna o melhor indivíduo a população
    vector<Elemento>* criaIndividuo();                             // Gera um indivíduo válido (vetor de listas)





    Ag(int geracao, double mutacao, double recombinacao, vector<Docente> *docentes_disponiveis,
       vector<Estudante> *estudantes_disponiveis, vector<Sala> *salas_disponiveis,vector<Disciplina> disciplinas, string** timeslots);
    virtual ~Ag();

    //=================== Getters and Setters ==============================

    int getGeracao() const;
    void setGeracao(int geracao);
    double getMutacao() const;
    void setMutacao(double mutacao);
    double getRecombinacao() const;
    void setRecombinacao(double recombinacao);

    int getNum_individuos() const;

    void setNum_individuos(int num_individuos);

    vector<Docente> *getDocentes_disponiveis() const;

    void setDocentes_disponiveis(vector<Docente> *docentes_disponiveis);

    vector<Estudante> *getEstudantes_disponiveis() const;

    void setEstudantes_disponiveis(vector<Estudante> *estudantes_disponiveis);

    vector<Sala> *getSalas_disponiveis() const;

    void setSalas_disponiveis(vector<Sala> *salas_disponiveis);

    const vector<Cromossomo> &getPopulacao() const;

    void setPopulacao(const vector<Cromossomo> &populacao);


};


#endif //ALGORITMOGENETICO_AG_H
