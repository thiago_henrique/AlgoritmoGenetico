//
// Created by jordy on 15/04/17.
//
#include "Elemento.h"



Elemento::Elemento(const Disciplina &disciplina, const Sala &sala, const vector<Estudante> &estudantes,
                   const Docente &docente/*, const Turma &turma*/) : disciplina(disciplina), sala(sala),
                                                                 estudantes(estudantes), docente(docente)/*,
                                                                 turma(turma)*/ {}

Elemento::~Elemento() {

}

const Disciplina &Elemento::getDisciplina() const {
    return disciplina;
}

void Elemento::setDisciplina(const Disciplina &disciplina) {
    Elemento::disciplina = disciplina;
}

const Sala &Elemento::getSala() const {
    return sala;
}

void Elemento::setSala(const Sala &sala) {
    Elemento::sala = sala;
}

const vector<Estudante> &Elemento::getEstudantes() const {
    return estudantes;
}

void Elemento::setEstudantes(const vector<Estudante> &estudantes) {
    Elemento::estudantes = estudantes;
}

const Docente &Elemento::getDocente() const {
    return docente;
}

void Elemento::setDocente(const Docente &docente) {
    Elemento::docente = docente;
}
/*
const Turma &Elemento::getTurma() const {
    return turma;
}

void Elemento::setTurma(const Turma &turma) {
    Elemento::turma = turma;
}
*/
